
// Tests using mocha and assert
const assert = require('assert');
const sum = (a, b) => a + b;

describe('Testing', () => {
  it('demo', function() {
    let expectedResult = 20;
    let a = 10;
    let b = 10;
    let result = sum(a ,b);
    assert.equal(result, expectedResult);
  });
});
