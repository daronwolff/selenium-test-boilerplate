const webdriver = require('selenium-webdriver');
const webDriverUtil = require('selenium-webdriver/lib/until');

class SeleniumTester {

  constructor() {
    this.driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome());
    this.isOpen = false;
    this.browser = false;
  }

  openBrowser() {
    this.browser = this.driver.build();
    this.isOpen = true;
  }

  closeBrowser() {
    this.browser.quit();
  }

  maximize() {
    this.browser.manage().window().maximize();
  }

  isOpened(force=false) {
    if(!this.isOpen) {
      if(force === true) {
        this.open();
        return true;
      }
      throw Error('Browser is not open, execute method .open()');
    }
    return true;
  }

  navigateTo(url) {
    this.isOpened(true);
    return this.browser.get(url);
  }

  waitForElement(cssSelector) {
    return this.browser.wait(
      webDriverUtil.elementLocated(webdriver.By.css(cssSelector)
    ), 8000);
  }

  selectElement(cssSelector) {
    this.waitForElement(cssSelector);
    return this.browser.findElement(webdriver.By.css(cssSelector));
  }

  sendValues(cssSelector, value) {
    this.waitForElement(cssSelector);
    let element = this.selectElement(cssSelector);
    return element.sendKeys(value);
  }

  getAttribute(cssSelector, atribute, cb) {
    return this.selectElement(cssSelector).getAttribute(atribute)
    .then(data => cb(data))
    .catch(e => console.log(e)
    );
  }

  getText(cssSelector, cb) {
    return this.selectElement(cssSelector).getText()
    .then(data => cb(data))
    .catch(e => console.log(e)
    );
  }

  click(cssSelector) {
    let element = this.selectElement(cssSelector);
    element.click();
  }

}

module.exports = SeleniumTester;
