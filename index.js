const SeleniumTester = require('./driver');
const test = require('selenium-webdriver/testing');
const { expect } = require('chai');

const myDriver = new SeleniumTester();
const URL = 'http://mudarseamerida.com/';

describe('Running tests', function () {
  this.timeout(8 * 1000);

  beforeEach(() => myDriver.openBrowser());

  describe('Login page', function () {

    beforeEach(() => {
      myDriver.navigateTo(URL);
    });

    // Example 1, using getAttribute
    test.xit('Can fill up text field', () => {
      let textDemo = 'demos';
      myDriver.sendValues('#s', textDemo);
      myDriver.getAttribute('#s', 'value', (text) => {
        return expect(text).to.be.equal(textDemo);
      });
    });

   // Example 2, using getText
    test.xit('E-mail addres is updated', () => {
      let title = 'contacto@mudarseamerida.com';
      myDriver.getText('.email-header-top', (text) => {
        return expect(text).to.be.equal(title);
      });
    });

    // Example 3, using getText and click
    test.it('Can search and find results', () => {
      let term = 'Residencia en Venta -Zona Cholul';
      myDriver.sendValues('#s', term);
      myDriver.click('#searchsubmit');
      myDriver.getText('.property-information-address', (text) => {
        return expect(text).to.be.equal(term);
      });
    });
    // Close browser
    afterEach(() => myDriver.closeBrowser());
  });
});
